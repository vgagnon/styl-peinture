import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ModalController  } from 'ionic-angular';

//Providers
import { ColorFormulaProvider } from '../../providers/color-formula';
import { GlobalProvider } from '../../providers/global-provider';
import { ProductsProvider } from '../../providers/products';
import {PrintProvider} from '../../providers/print';
import {PrinterListModalPage} from '../printer-list-modal/printer-list-modal';

import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-view-color',
  templateUrl: 'view-color.html',
})
export class ViewColorPage {
  color: any;
  details: any;
  showSpinner = true;
  editDetails = false;
  editingDetail = "";
  calculating = false;
  calculated = false;
  modifRatio = 1;
  newDetail = {
    Colordetail_ID: "",
    Color_ID: "",
    product_ID: "",
    productName: "",
    quantite: "",
    unite: "L",
    product: "",
  }
  noDetails = false;
  units = ["L", "g", "goutte(s)", "mL", "oz"];
  selectedPrinter:any=[];


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formulaService: ColorFormulaProvider,
              public productService: ProductsProvider,
              public globalService: GlobalProvider,
              public events: Events,
              private alertCtrl: AlertController,
              private modalCtrl:ModalController,
              private printProvider:PrintProvider) {

              events.subscribe('color:edited', (color) => {
                this.color = color;
              });
  }

  ionViewDidLoad() {
    if(this.navParams.get('color')){
      this.color = this.navParams.get('color')
      this.newDetail.Color_ID = this.color.Color_ID;
      console.log(this.color);
      this.getDetails();
    }
    else if(this.navParams.get('colorID')){
      this.formulaService.getByID(this.navParams.get('colorID')).then(color => {
        this.color = color;
        this.newDetail.Color_ID = this.color.Color_ID;
        this.getDetails();
      }, err => {
        this.globalService.showToast("Erreur lors de la requête de la couleur");
      });
    }
    this.getPassword();
  }

  getDetails(){
    this.formulaService.getDetailsByFormulaID(this.color.Color_ID).then(details => {
      this.details = details;
      if(this.details.length > 0){
        this.noDetails = false;
        for(let i=0; i < this.details.length; i++){

          this.productService.getByID(this.details[i].product_ID).then(product => {
            this.details[i].product = product;
            this.showSpinner = false;
          }, err => {
            console.log(err);
            this.globalService.showToast("Erreur lors de la requête des détails");
          });
        }
        console.log(this.details);
      }
      else{
        this.noDetails = true;
        this.showSpinner = false;
      }
    }, err => {
      console.log(err);
      this.globalService.showToast("Erreur lors de la requête des détails");
    });
  }

  deleteColor(){
    this.formulaService.deleteColor(this.color.Color_ID).then(() => {
        console.log('color deleted');
        this.events.publish('colors:reload');
        this.navCtrl.pop();
        this.globalService.showToast("Couleur supprimée avec succès");
      }, err => {
        this.globalService.showToast("Erreur lors du retrait de la couleur");
    });
  }

  presentPrompt(type) {
    let alert = this.alertCtrl.create({
      title: type + " couleur",
      subTitle: 'Mot de passe nécessaire pour continuer.',
      inputs: [
        {
          name: 'password',
          placeholder: 'Mot de passe',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: type,
          handler: data => {
            if (data.password == this.globalService.password) {
              if(type == "Supprimer"){
                this.deleteColor();
              }
              else if(type == "Modifier"){
                this.navCtrl.push('AddColorPage', {color: this.color})
              }
            } else {
              this.globalService.showToast("Mot de passe invalide");
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  toggleEditDetails(){
    if(this.editDetails){
      this.editDetails = false
      this.editingDetail = "";
      this.newDetail.productName = "";
      this.newDetail.product_ID = "";
      this.newDetail.quantite = "";
      this.newDetail.unite = "L";
      for(let i=0; i<this.details.length; i++){
        this.details[i].edit = false;
      }
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Modifier Recette',
        subTitle: 'Mot de passe nécessaire pour continuer.',
        inputs: [
          {
            name: 'password',
            placeholder: 'Mot de passe',
            type: 'password'
          }
        ],
        buttons: [
          {
            text: 'Annuler',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Confirmer',
            handler: data => {
              if (data.password == this.globalService.password) {
                this.editDetails = true
              } else {
                this.globalService.showToast("Mot de passe invalide");
                return false;
              }
            }
          }
        ]
      });
      alert.present();
    }
  }

  selectProduct(){
    let productsModal = this.modalCtrl.create('ProductsPage', { selectMode: true });
    productsModal.onDidDismiss(product => {
      this.newDetail.product_ID = product.ProductID;
      this.newDetail.productName = product.ProductNamestyl;
      this.newDetail.product = product;
      console.log(this.newDetail);
    });
    productsModal.present();
  }

  toggleEditDetail(detail){
    if(detail.edit){
      detail.edit = false;
      this.editingDetail = "";
      this.newDetail.productName = "";
      this.newDetail.product_ID = "";
      this.newDetail.quantite = "";
      this.newDetail.unite = "L";
    }
    else{
      for(let i=0; i<this.details.length; i++){
        this.details[i].edit = false;
      }
      detail.edit = true;
      this.editingDetail = detail.Colordetail_ID;
      this.newDetail.Colordetail_ID = detail.Colordetail_ID;
      this.newDetail.productName = detail.productName;
      this.newDetail.product_ID = detail.product_ID;
      this.newDetail.quantite = detail.quantite;
      this.newDetail.productName = detail.product.ProductNamestyl;
      this.newDetail.unite = detail.unite.replace(/ +/g, "");
    }

  }

  saveDetail(){
    let approved = false;
    if(this.newDetail.product_ID != "" && this.newDetail.quantite != "" && this.newDetail.unite != ""){
      console.log(this.newDetail);
      if(this.editingDetail == ""){
        this.formulaService.addDetail(this.newDetail).then(() => {
          this.editingDetail = "";
          this.newDetail.productName = "";
          this.newDetail.product_ID = "";
          this.newDetail.quantite = "";
          this.newDetail.unite = "L";
          for(let i=0; i<this.details.length; i++){
            this.details[i].edit = false;
          }
          this.getDetails();
        }, err => {
          this.globalService.showToast("Erreur lors de l'ajout");
        });
      }
      else{
        this.formulaService.updateDetail(this.newDetail).then(() => {
          this.editingDetail = "";
          this.newDetail.productName = "";
          this.newDetail.product_ID = "";
          this.newDetail.quantite = "";
          this.newDetail.unite = "L";
          for(let i=0; i<this.details.length; i++){
            this.details[i].edit = false;
          }
          this.getDetails();
        }, err => {
          this.globalService.showToast("Erreur lors de l'ajout");
        });
      }

    }
    else{
      this.globalService.showToast('Élément de recette incomplet')
    }
  }

  

  deleteDetail(){
    let alert = this.alertCtrl.create({
      title: 'Supprimer?',
      subTitle: 'Êtes-vous certain de vouloir supprimer ce produit?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmer',
          handler: data => {
            this.formulaService.deleteDetail(this.editingDetail).then(() => {
              this.editingDetail = "";
              this.newDetail.productName = "";
              this.newDetail.product_ID = "";
              this.newDetail.quantite = "";
              this.newDetail.unite = "L";
              for(let i=0; i<this.details.length; i++){
                this.details[i].edit = false;
              }
              this.getDetails();
            }, err => {
              this.globalService.showToast("Erreur lors de la procédure");
            });
          }
        }
      ]
    });
    alert.present();
  }

  getPassword(){
    this.globalService.getPassword().then(() => {
    }, err => {
      this.globalService.showToast("Mot de passe inaccessible");
    });
  }

  toggleCalculator(){
    if(this.calculating){
      this.calculating = false
      this.calculated = false
      for(let i=0; i<this.details.length; i++){
        this.details[i].reference = false;
        this.details[i].newQuantity = 0;
      }
    }
    else{
      this.calculating = true
    }
  }

  selectBase(detail){
    if(this.calculating){
      let message = detail.product.ProductNamestyl + ' choisi. La quantité originale est de ' + detail.quantite + detail.unite.replace(/ +/g, "");
      let alert = this.alertCtrl.create({
        title: 'Nouvelle quantité',
        message: message,
        inputs: [
          {
            name: 'newQty',
            placeholder: 'Entrer nouvelle quantité',
            type: 'number'
          }
        ],
        buttons: [
          {
            text: 'Annuler',
            role: 'cancel'
          },
          {
            text: 'Confirmer',
            handler: data => {
              let ratio = data.newQty/detail.quantite;
              this.modifRatio = ratio;
              for(let i=0; i<this.details.length; i++){
                this.details[i].reference = false;
                this.details[i].newQuantity = ratio * this.details[i].quantite;
              }
              detail.reference = true;
              this.calculated = true;
            }
          }
        ]
      });
      alert.present();
    }
  }

  print(){
    let content = this.color.Color_name + "\n" + "ID: " + this.color.Color_ID + "\n";
    if(this.calculated && this.calculating){
      content = content + "**MODIF, ratio: " + this.modifRatio + '\n';
    }
    content = content + "\n";

    for(let i = 0; i< this.details.length; i++){
      let detailName = this.details[i].product.ProductNamestyl + ': ';
      if(this.calculated && this.calculating){
        let detailQty = this.details[i].newQuantity + ' ' + this.details[i].unite + "\n";
        content = content + detailName + detailQty;
      }
      else{
        let detailQty = this.details[i].quantite + ' ' + this.details[i].unite  + "\n";
        content = content + detailName + detailQty;
      }
    }

    content = content + '\n'+  'Date: ' + String(moment.utc(Date()).format('L')) + '\n\n\n\n';
    console.log(content);

    this.printProvider.searchBt().then(datalist=>{

      let abc=this.modalCtrl.create(PrinterListModalPage,{data:datalist});  
      abc.onDidDismiss(data=>{
        this.selectedPrinter=data;
        console.log(data);
        var id=this.selectedPrinter.id;

        this.printProvider.print(id, content);

      });
      
      //0. Present Modal
      abc.present();

    },err=>{
      console.log("ERROR",err);
      let mno=this.alertCtrl.create({
        title:"ERROR "+err,
        buttons:['Dismiss']
      });
      mno.present();
    })

  }
}

