import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewColorPage } from './view-color'

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ViewColorPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewColorPage),
    PipesModule
  ],
})
export class ViewColorPageModule {}
