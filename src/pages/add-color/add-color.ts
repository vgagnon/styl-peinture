import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Events } from 'ionic-angular';

//Providers
import { ColorFormulaProvider } from '../../providers/color-formula';
import { GlobalProvider } from '../../providers/global-provider';

@IonicPage()
@Component({
  selector: 'page-add-color',
  templateUrl: 'add-color.html',
})
export class AddColorPage {
  @ViewChild(Slides) slides: Slides;
  editMode = false;
  color: any = {
    Actif: true,
    Color_description: "",
		Color_name: "",
		Customer_name: "",
		Date: new Date().toISOString(),
		Glaze: false,
		Job_ref: "",
		Localisation: "",
		PO_customer: "",
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formulaService: ColorFormulaProvider,
              public globalService: GlobalProvider,
              public events: Events,) {
  }

  ionViewDidLoad() {
    if(this.navParams.get('color')){
      this.editMode = true;
      this.color = this.navParams.get('color');
    }
  }

  saveColor(){
    if(this.editMode){
      this.formulaService.update(this.color).then(() => {
        this.events.publish('colors:reload');
        this.events.publish('color:edited', this.color);
        this.navCtrl.pop();
      }, err => {
        console.log(err);
      });
    }
    else{
      this.formulaService.create(this.color).then(colorID => {
        console.log(colorID);
        this.events.publish('colors:reload');
        this.navCtrl.pop();
        this.navCtrl.push('ViewColorPage', {colorID: colorID});
      }, err => {
        console.log(err);
      });
    }
  }
}
