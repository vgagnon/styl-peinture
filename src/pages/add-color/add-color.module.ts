import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddColorPage } from './add-color';

@NgModule({
  declarations: [
    AddColorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddColorPage),
  ],
})
export class AddColorPageModule {}
