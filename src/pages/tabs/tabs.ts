import { Component } from '@angular/core';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'ColorsPage';
  tab2Root = 'ProductsPage';

  constructor() {

  }
}
