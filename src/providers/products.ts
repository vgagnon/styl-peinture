import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

//Providers
import { serverAddresses } from './server.config';

@Injectable()
export class ProductsProvider {
  server = serverAddresses.api;
  products: any[];

  constructor(public http: HttpClient) {
    console.log('Hello ProductsProvider Provider');
  }

  getAll(){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/products/all', { headers })
        .subscribe((formulas: any) => {
          this.products = formulas;
          resolve(formulas);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

  getByID(productID){
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders()

      this.http.get(this.server + '/products/id/'  + productID, { headers })
        .subscribe((product: any) => {
          resolve(product);
        }, (err) => {
          //this.loadingJamsError = true;
          reject(err);
        });
    });
 
  }

}
