import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//Ionic Native
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';

import { serverAddresses } from './server.config';

@Injectable()
export class GlobalProvider {
    server = serverAddresses.api;
    devMode = false;
    networkConnected = true;
    deviceModel: any;
    password = "styl";
    appVersion = {
        string: "1.0.0",
        number: "10000"
    };
    noInternetToast: any;

    constructor(private toastCtrl: ToastController,
                private platform: Platform,
                private network: Network,
                private device: Device,
                public http: HttpClient){
        
        platform.ready().then(() => {
            if(this.network.type == "none"){
                this.networkConnected = false;
                this.showNoConnectionToast();
            }

            this.deviceModel = this.device.model;

            let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
                if(this.networkConnected){
                    this.showNoConnectionToast();
                }
                    this.networkConnected = false;
            });

            let connectSubscription = this.network.onConnect().subscribe(() => {
                this.networkConnected = true;
                this.noInternetToast.dismiss();
            });
        });   
    }

    showNoConnectionToast(){
        let noInternetMessage = "Pas de connection internet";
        let closeText = "fermer";
        this.noInternetToast = this.toastCtrl.create({
            message: noInternetMessage,
            showCloseButton: true,
            duration: 3000,
            closeButtonText: closeText,
            position: 'top'
            });

        this.noInternetToast.present();
            
    }

    //Show message
    showToast(message: string, duration = 3000, showCloseButton = false, dismissOnPageChange = false) {
      let closeButtonText = "fermer";

      let toast = this.toastCtrl.create({
          message: message,
          duration: duration,
          position: 'bottom',
          showCloseButton: showCloseButton,
          closeButtonText: closeButtonText,
          dismissOnPageChange: dismissOnPageChange
      });
      toast.present();
    }

    getPassword(){
        return new Promise((resolve, reject) => {
          let headers = new HttpHeaders()
    
          this.http.get(this.server + '/password', { headers })
            .subscribe((password: any) => {
                this.password = password.replace(/ +/g, "");
              resolve(password);
            }, (err) => {
              //this.loadingJamsError = true;
              reject(err);
            });
        });
     
    }
}
