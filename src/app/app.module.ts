import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';
import { Printer, PrintOptions } from '@ionic-native/printer';

import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { ColorFormulaProvider } from '../providers/color-formula';
import { ProductsProvider } from '../providers/products';
import { GlobalProvider } from '../providers/global-provider';
import { serverAddresses } from '../providers/server.config';
import { PrintProvider } from '../providers/print';

import {PrinterListModalPage} from '../pages/printer-list-modal/printer-list-modal';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    PrinterListModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    }),
    BrowserModule,
    HttpClientModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    PrinterListModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ColorFormulaProvider,
    ProductsProvider,
    GlobalProvider,
    Network,
    Device,
    Printer,
    PrintProvider,
    BluetoothSerial
  ]
})
export class AppModule {}
